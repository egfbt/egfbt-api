var axios = require('axios');


const getPost = (call, data) => {
	var headers = {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
		'token': 'ac1dd209cbcc5e5d1c6e28598e8cbbe8',
		'username': 'scook'
	};

	return axios({
		method: 'post',
		url: `http://localhost:9999/${call}`,
		data: data,
		headers: headers
	});
}

getPost('getMyCookie', {cookie: 'monster'}).then((res)=>{
	console.log(res.data);
}).catch((err) => {
	console.error(`${err.response.status}: ${err.response.statusText}`);
});


