
class ResponseError extends Error{

	constructor(code, message, details = message){
		super(message);
		this.code = code;
		this.details = details;
	}

	getCode(){
		return this.code;
	}

	getMessage(){
		return this.message;
	}

	getDetails(){
		if(this.message == this.details) return this.message;
		return this.message + " > " + this.details;
	}
}

//Note that details are logged but not sent back
module.exports = {
	ResponseError,
	BadRequest: class extends ResponseError{constructor(message, details = message){ super(400, message, details);}},
	AccessDenied: class extends ResponseError{constructor(message, details = message){ super(401, message, details);}},	
	NotFound: class extends ResponseError{constructor(message, details = message){ super(404, message, details);}},	
	ForBidden: class extends ResponseError{constructor(message, details = message){ super(403, message, details);}},	

}

/**
 * HTTP Error Code and their definition

301 - Moved Permanently
302 - Object moved.
304 - Not modified.
307 - Temporary redirect.

403.1 - Execute access forbidden.
403.2 - Read access forbidden.
403.3 - Write access forbidden.
403.4 - SSL required.
403.5 - SSL 128 required.
403.6 - IP address rejected.
403.7 - Client certificate required.
403.8 - Site access denied.
403.9 - Too many users.
403.10 - Invalid configuration.
403.11 - Password change.
403.12 - Mapper denied access.
403.13 - Client certificate revoked.
403.14 - Directory listing denied.
403.15 - Client Access Licenses exceeded.
403.16 - Client certificate is untrusted or invalid.
403.17 - Client certificate has expired or is not yet valid.
403.18 - Cannot execute requested URL in the current application pool. This error code is specific to IIS 6.0.
403.19 - Cannot execute CGIs for the client in this application pool. This error code is specific to
403.20 - Passport logon failed. This error code is specific to IIS 6.0.
404.0 - (None) – File or directory not found.
404.1 - Web site not accessible on the requested port.
404.2 - Web service extension lockdown policy prevents this request.
404.3 - MIME map policy prevents this request.
405 - HTTP verb used to access this page is not allowed (method not allowed.)
406 - Client browser does not accept the MIME type of the requested page.
407 - Proxy authentication required.
412 - Precondition failed.
413 – Request entity too large.
414 - Request-URI too long.
415 – Unsupported media type.
416 – Requested range not satisfiable.
417 – Execution failed.
423 – Locked error. 
500 - Internal server error.
500.12 - Application is busy restarting on the Web server.
500.13 - Web server is too busy.
500.15 - Direct requests for Global.asa are not allowed.
500.16 – UNC authorization credentials incorrect. This error code is specific to IIS 6.0.
500.18 – URL authorization store cannot be opened. This error code is specific to IIS 6.0.
500.19 - Data for this file is configured improperly in the metabase.
500.100 - Internal ASP error.
501 - Header values specify a configuration that is not implemented.
502 - Web server received an invalid response while acting as a gateway or proxy.
502.1 - CGI application timeout.
502.2 - Error in CGI application.
503 - Service unavailable. This error code is specific to IIS 6.0.
504 - Gateway timeout.
505 - HTTP version not supported.

 */
