const Session = require('express-session');
const KeyCloak = require('keycloak-connect');
const Express = require('express');
const CORE = require('we-node-core');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const ApiMapper = require('./apiMapper');
const https = require('https');
const FS = require('fs');
const CORS = require('cors');

module.exports = async (config) => {

	const logger = CORE.getLogger(`${config.realm}-sys`);

	logger.info('Initializing Express');
	var app = Express();
	app.use(bodyParser.json());
	app.use(cookieParser());
	app.use(CORS());
	app.options('*',CORS());

	logger.info('Configuring CORS');
	app.use(async (req, res, next) => {
		res.setHeader('Access-Control-Allow-Origin', '*'); 
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
		res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, connect.sid');
		res.setHeader('Access-Control-Allow-Credentials', true);
		next();
	});

	logger.info('Initializing Session');
	var memoryStore = new Session.MemoryStore();
	app.use(Session({...config.session, store: memoryStore} ));

	logger.info('initializing keycloak');
	var keycloak = new KeyCloak({store: memoryStore}, config.keycloak);
	app.use( keycloak.middleware() );

	config.systems = {
		keycloak,
		app,
		logger
	};
	
	logger.info('Loading API interfaces');
	const mapper = ApiMapper.getMapper(config);
	mapper.buildAPIs();

	logger.info('Configuring static resources');
	app.use(Express.static(config.publicPath, {maxAge: '1h'}));
	app.use(
		'/secure',
		keycloak.protect(), 
		Express.static(	config.securePath, {maxAge: '1h'}), 
	);

	app.get('/authRelay',
		(req, resp, next) => {
			//Roles are not currently working.  Need to work on that part.
			var roles = (req.query.roles)? req.query.roles.split(','): [];
			//console.log(roles);
			if(roles.length == 1)roles = roles[0];
			else if(roles.length == 0) roles = null;
			keycloak.protect(roles)(req, resp, next);
			if(req.kauth.grant){
				console.info(req.kauth.grant.access_token.content);
			}
			else{
				//console.log('eek');
			}

		}, 
		(req, resp) => {
			console.log('hmmm');
			resp.cookie('token',req.cookies['connect.sid']);
			resp.writeHead(301, {Location: req.query.return});
			resp.send();
			var {params, query, cookies} = req;
			console.log({params, query, cookies});
		}
	);

	app.post('/authRelay',
		(req, resp, next) => {
			//Roles are not currently working.  Need to work on that part.
			var roles = (req.query.roles)? req.query.roles.split(','): [];
			//console.log(roles);
			if(roles.length == 1)roles = roles[0];
			else if(roles.length == 0) roles = null;
			keycloak.protect(roles)(req, resp, next);
			next();
			if(req.kauth.grant){
				console.info(req.kauth.grant.access_token.content);
			}
			else{

			}

		}, 
		(req, resp) => {
			if(req.kauth.grant.access_token.content){
				resp.send(req.kauth.grant.access_token.content);
			}
			else resp.status(500).send('Unepected strange issue took place');
		}
	);


	logger.info(`Starting API on port ${config.port}`);
	https.createServer({
		cert: FS.readFileSync( systemConfig.security.cert ),   
		key: FS.readFileSync( systemConfig.security.key ),    
	}, app )
		.listen(config.port, () => console.log(`${config.realm} is securely listening on port ${config.port}`));
	while(true) {
		CORE.logSystemStatus(config.realm, 'Active');
		await CORE.sleep(1 * 60 *60 * 1000);
	}

}