const CORE = require('we-node-core');
const {validate, handleRequest, validateIP, setConfig} = require('./handlers');


const mergeDef = (def, method) => {
	var mdef = (typeof def[method] == 'function') ? {handler: def[method]}: def[method];		
	if(!mdef.handler) throw new Error(`Setup error, ${method} defined, but is missing handler`);
	if(!mdef.security) mdef.security = def.security;
	if(!mdef.schema) mdef.schema = def.schema;	
	if(!mdef.iplimit) mdef.iplimit = def.iplimit;
	mdef.method = method;
	return mdef;
}

const METHODS = ['get','post','put'];


class RegisterAgent{

	constructor(config){
		this.config = config;
	}

	buildDef(def){
		Object.keys(def).forEach(method => {
			if(!METHODS.includes(method)) return;

			const logger = CORE.getLogger(`${this.config.realm}-${def.path}-${method}`);

			var mdef = mergeDef(def, method);
			mdef.logger = logger;
			var funChain = [setConfig(mdef)];
			if(mdef.security && mdef.security.byRole){
				funChain.push(this.config.systems.keycloak.protect(mdef.security.roles));
			}
			if(mdef.security && mdef.security.byIP){
				funChain.push(validateIP(mdef.security));
			}
			funChain.push(validate(mdef.schema));

			const handler = async(req, resp) => {
				await handleRequest(req, resp, mdef);
			};

			switch(method){
				case 'get': this.config.systems.app.get(def.path, funChain, handler); break;
				case 'post': this.config.systems.app.post(def.path, funChain, handler); break;
				case 'put': this.config.systems.app.put(def.path, funChain, handler); break;
			}
		});
	}	
}

module.exports = {
	getAgent: (config) => {
		return new RegisterAgent(config);
	}
}