const FS = require('fs');
const UTIL = require('util');
const PATH = require('path');
const registerAPI = require('./registerAPI');


class APIMapper{
	constructor(config){
		this.config = config;
		this.logger = config.systems.logger;
		this.apiPaths = new Map;
		this.apiPaths.set('/authRelay','CORE');
	}

	buildAPIs(){
		this.logger.info(`Loading APIs: ${this.config.apiPath}`);
		if(!FS.existsSync(this.config.apiPath)) {
			throw new Error(`${this.config.apiPath} is not valid`);
		}
		this.agent = registerAPI.getAgent(this.config);
		this.loadAPI(this.config.apiPath);
		this.logger.info(`APIs loaded`);
	}

	loadAPIDir(path){
		const fdir = FS.readdirSync(path);
		for(var i = 0; i < fdir.length; i++) {
			this.loadAPI(path + '/' + fdir[i]);
		}
	}

	loadAPIFile(path){
		if(!path.includes('.js')) return; //Skip non-JS files
		var assumedPath = path.replace(this.config.apiPath,'').replace('.js','');
		var def = require(PATH.resolve(path));	
		if(def.path === undefined) def.path = assumedPath;
		if(this.apiPaths.has(def.path)) {
			throw new Error(`Path ${def.path} is already defined in ${this.apiPaths.get(def.path)} cannot reset for file ${path}.  Try not setting it specifically`);
		}
		this.apiPaths.set(def.path, path);
		this.logger.info(`Build Interfaces for ${def.path}`);
		this.agent.buildDef(def);
	}

	loadAPI(path){
		var stat = FS.lstatSync(path);
		if(stat.isDirectory()) {
			this.loadAPIDir(path);
		}
		else if(stat.isFile()) {
			this.loadAPIFile(path);
		}
		else{
			var msg = UTIL.format(stat);
			throw new Error(`${path} is not a file nor directory:\n${msg}`);
		}
	
	}
	
}	
	
module.exports = {
	getMapper: (config) => {
		return new APIMapper(config);
	}
}

