const ERRORS = require('../ResponseErrors');
const VALIDATOR = require('fastest-validator');
const util = require('util');

const getIpAddr = require('ipware')().get_ip;

const validator = new VALIDATOR();
const internalIP = [
	/^192\.(?:[0-9]{1,3}\.){2}[0-9]{1,3}$/, 
	/^10\.5\.(?:[0-9]{1,3}\.){1}[0-9]{1,3}$/,
	/localhost/,
];

exports.validate = (schema) => {
	return (req, resp, next) => {
		if(!schema) {
			next();
		} 

		//--We need to get the req body here, RBW
		//--if we want to use query string params then use "req.query", RBW
		//var parms = (req.method == 'GET')? req.params : req.body;
		var parms = req.body; 
		//console.log({parms, params: req.body});
		//console.log({parms, params: req.params});

		var results = validator.validate(parms, schema);
		if(results == true) {
			Object.keys(schema).forEach(key => {
				if(schema[key] instanceof Object) {
					if(schema[key].uppercase) {
						parms[key] = parms[key].toUpperCase();
					}
				}
			});
			next();
			return;
		} else {
			resp.status(400).send(results);
		}
	}
}

const validateIP = (ipAddress, ipListing) => {
	ipListing.forEach(element => {
		if(element.test(ipAddress)) {
			return true;
		} 
	});
	return false;
}

exports.validateIP = (sec) => {
	
	const validIPS = (sec.internal)? internalIP: [];
	if(sec.ipLimit) {
		var ips = (Array.isArray(sec.ipLimit))? sec.ipLimit: [sec.ipLimit];
		ips.forEach(ip => {
			if(!ip instanceof RegExp) throw new Error('Cannot create IP Validation, ip limits must be regular expression objects');
			validIPS.push(ip);
		});
	}

	return (req, resp, next) => {
		req.logger.debug(`validateIP ${resp.headersSent}`);
		req.ipAddress = getIpAddr(req).clientIp.replace("::ffff:", "").replace("::1", "localhost");
		if(validateIP(req.ipAddress, validIPS)) next();
		resp.status(404).send('This file does exist in this universe.', 'Invalid IP Address for resource');
	}
}

exports.setConfig = (def) => {
	return (req, resp, next) => {
		req.logger = def.logger;
		next();
	}
}

exports.handleRequest = async (req, resp, mdef) => {
	req.logger.debug(`setConfig ${resp.headersSent}`);
	req.user = req.kauth.grant; //TODO: Need to refine this
	req.logger.log(req.user);

	try {	
		var result = await mdef.handler(req, resp);

		//--this line causing errors, 02/24/2020, RBW
		//resp.send(require('util').format(resp));
		//--commented out for now, 02/24/2020, RBW

		if(!resp.headersSent) {
			req.logger.log(result);
			resp.status(200).send(result);
			req.logger.log(result);
		}
	}
	catch(err) {
		if(err instanceof ERRORS.ResponseError) {
			req.logger.warn(`Error Response: (${err.getCode()}) - ${err.getDetails()}`);
			if(!resp.headersSent) resresp.status(err.getCode()).send(err.getMessage());
		} else {
			req.logger.error(`Unspecified Error: `, err);
			if(!resp.headersSent) resp.status(500).send(err.toString());
		}
	}
}