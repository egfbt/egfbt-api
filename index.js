'use strict';

/*

Usage
Extend out CORE functions as well.
var server = require('egfbt-api');

server.addRealm(
    

module.exports = {
	port: 6443,
	keycloak: {
		"realm": "Office",
		"auth-server-url": "http://localhost:8080/auth/",
		"ssl-required": "external",
		"resource": "play",
		"credentials": {
		  "secret": "2a924fba-f0c7-4dcc-a570-e2cbc204b973"
		},
		"confidential-port": 0
	}
}

const PATH = require('path');

{
    realm: 'default',
    security: ['keycloak', {
		"realm": "Office",
		"auth-server-url": "http://localhost:8080/auth/",
		"ssl-required": "external",
		"resource": "play",
		"credentials": {
		  "secret": "2a924fba-f0c7-4dcc-a570-e2cbc204b973"
		},
		"confidential-port": 0
	}],
    validation: [],

	port: 3443,
	keycloak: ,
	session: {
		secret: 'some secret',
		resave: false,
		saveUninitialized: true,
	},
	apiPath: PATH.join(__dirname, '/api'), 
	publicPath: PATH.join(__dirname, '/public'),
	securePath: PATH.join(__dirname, '/secure')
	
}

);
server.setConfig('./config');

etl

server.start();

*/

const CORE = require('we-node-core');


const engine = require('./engine');



CORE.setDomainPath(__dirname + '/domains');
const engine = require('./engine');

//Initialize Drivers -SHould refactor core to remove the need for this.
require('we-db2');
require('we-communicator');


const startup = (config) => {
    engine(config)
        .then(res => {
            console.error('unexpectedly exited with results: ' + res);
        })
        .catch(err => {
            console.error(err);
        });

}


const start = async() =>{
    await CORE.init();
    CORE.interceptOn();
    const logger = CORE.getLogger('Initialization');

//    startup(require('./realms/claims'));
    startup(require('../api/node/realms/default'));
//    startup(require('./realms/driver'));
    startup(require('../api/node/realms/office'));
//    startup(require('./realms/vendor'));

}

start();